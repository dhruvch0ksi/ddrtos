#pragma once

#include "uart.h"
#include <stdint.h>

typedef enum {
  sound_effect__mp3 = 2,
  song__mp3 = 3,
} mp3_select_e;

void mp3_module__init(uart_e uart_to_use);
void mp3_module__play(uart_e uart_to_use, uint32_t song_number);
void mp3_module__resume(uart_e uart_to_use);
void mp3_module__pause(uart_e uart_to_use);
void mp3_module__reset_playback(uart_e uart_to_use);