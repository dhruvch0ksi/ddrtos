#pragma once

#include "FreeRTOS.h"
#include "task.h"

TaskHandle_t title_logo_sequence;

void title_logo(void *p);

void press_to_start__handler(void);