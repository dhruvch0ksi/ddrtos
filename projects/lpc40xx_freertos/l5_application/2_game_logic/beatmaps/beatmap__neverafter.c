#include "beatmap__neverafter.h"

#include "led_matrix.h"
#include "symbols.h"

void beatmap__neverafter(void *p) {
  const uint16_t length_of_one_beat__ms = 699;
  TickType_t xLastNoteEndTime;

  while (1) {

    for (uint8_t i = 0; i < 16; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_one_beat__ms);
    }

    for (uint8_t i = 0; i < 8; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_one_beat__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_one_beat__ms);
    }

    const uint16_t length_of_first_pause__ms = 340;
    const uint16_t length_of_second_pause__ms = 530;
    const uint16_t length_of_third_pause__ms = 170;
    const uint16_t length_of_fourth_pause__ms = 355;

    for (uint8_t i = 0; i < 16; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_first_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_second_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_third_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_fourth_pause__ms);
    }

    vTaskDelay(100); // Comp
    const uint16_t length_of_bridge_pause__ms = 500;
    const uint16_t length_of_bridge_pause_long__ms = 2581;

    for (uint8_t i = 0; i < 3; i++) {
      // IN ORDER MELODY
      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);

      // OUT OF ORDER MELODY

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);
    }

    const uint16_t length_of_bridge_pause_long_special__ms = length_of_bridge_pause_long__ms - 300;

    // IN ORDER MELODY
    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special__ms);

    // OUT OF ORDER MELODY WITH EARLY NOTE

    const uint16_t length_of_bridge_pause_long_special_remainder__ms = 300;

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special_remainder__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);

    ///////////////////////////////// CHORUS HERE ///////////////////////////////

    // IN ORDER MELODY
    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special__ms);

    // OUT OF ORDER MELODY WITH EARLY NOTE
    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special_remainder__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special__ms);

    for (uint8_t i = 0; i < 2; i++) {
      // IN ORDER MELODY WITH EARLY NOTE
      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special_remainder__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special__ms);

      // OUT OF ORDER MELODY WITH EARLY NOTE
      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special_remainder__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special__ms);
    }

    // IN ORDER MELODY WITH EARLY NOTE
    xLastNoteEndTime = xTaskGetTickCount();
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special_remainder__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special__ms);

    // OUT OF ORDER MELODY WITH EARLY NOTE
    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special_remainder__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);

    // Lag compensate
    vTaskDelay(200);

    ////// CHORUS PART 2 GOES HERE //////////
    for (uint8_t i = 0; i < 4; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_first_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_second_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_third_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_fourth_pause__ms);
    }

    for (uint8_t i = 0; i < 4; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_first_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_second_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_third_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_fourth_pause__ms);
    }

    for (uint8_t i = 0; i < 4; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_first_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_second_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_third_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_fourth_pause__ms);
    }

    for (uint8_t i = 0; i < 4; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_first_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_second_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_third_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_fourth_pause__ms);
    }
    ///////////////////////////////// END OF CHORUS ///////////////////////////////
    const uint16_t eight_bar_gap = 11162;
    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, eight_bar_gap);

    for (uint8_t i = 0; i < 2; i++) {
      // IN ORDER MELODY
      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);

      // OUT OF ORDER MELODY

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);

      vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);
    }

    // IN ORDER MELODY
    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special__ms);

    // OUT OF ORDER MELODY WITH EARLY NOTE

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long_special_remainder__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);

    ////////////////////////////////// START CHORUS 2 //////////////////////////////////////
    // Lag compensate
    vTaskDelay(100);

    const uint16_t length_of_chorus_two_pause_1__ms = 350; // 480
    const uint16_t length_of_chorus_two_pause_2__ms = 100; // 400
    const uint16_t length_of_chorus_two_pause_3__ms = 1296;
    const uint16_t length_of_chorus_two_pause_4__ms = 218;
    const uint16_t length_of_chorus_two_pause_5__ms = 698;

    for (uint8_t i = 0; i < 4; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__up_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_1__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_2__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_3__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_4__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_3__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_4__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_3__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_4__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_5__ms);
    }

    for (uint8_t i = 0; i < 4; i++) {
      xLastNoteEndTime = xTaskGetTickCount();
      draw__down_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_1__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_2__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_3__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_4__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_3__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_4__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_3__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__right_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_4__ms);

      xLastNoteEndTime = xTaskGetTickCount();
      draw__left_arrow_bottom__static(White);
      vTaskDelayUntil(&xLastNoteEndTime, length_of_chorus_two_pause_5__ms);
    }

    ///////////// OUTRO ////////////
    vTaskDelay(100);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__right_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__down_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__up_arrow_bottom__static(White);

    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause__ms);

    xLastNoteEndTime = xTaskGetTickCount();
    draw__left_arrow_bottom__static(White);
    vTaskDelayUntil(&xLastNoteEndTime, length_of_bridge_pause_long__ms);

    vTaskSuspend(beatmap__neverafter__handler);
  }
}