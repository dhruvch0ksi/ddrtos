#include "game_logic.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

#include "beatmap__dont_sleep.h"
#include "beatmap__neverafter.h"
#include "beatmap__weightless.h"
#include "button.h"
#include "gpio.h"
#include "gpio0_isr.h"
#include "led_matrix.h"
#include "mp3_module.h"
#include "song_info.h"
#include "song_select.h"
#include "symbols.h"
#include "uart.h"

#include <stdlib.h>

static volatile uint32_t score = 0;
static volatile uint32_t multiplier = 1;
static uint8_t shift_row_delay;
static SemaphoreHandle_t switch_pressed_signal_sound;

/***** HELPER FUNCTIONS *****/
static void draw_arrow_outlines(color_code color);
static void erase_arrow_outlines(void);
static void resume_tasks_for_gameplay(uint8_t song_choice);
static void wait_for_arrows_to_reach_top_then_start_music(const uint32_t time_to_wait__ms, uart_e uart_number,
                                                          const uint8_t song_number);
static void wait_for_song_to_finish(uint16_t song_runtime__seconds);
static void suspend_game_logic_main_task_and_resume_song_select_task(void);

static void clear_score(void);
static void draw_score(void);
static void draw_multiplier(void);
static void update_score_and_multiplier(bool is_hit);
static void play_random_fail_sound(void) { mp3_module__play(sound_effect__mp3, (score % 2) + 2); }
static void clear_screen_bottom_to_top(void);

void game_logic(void *p) {
  switch_pressed_signal_sound = xSemaphoreCreateBinary();
  while (1) {
    set__game_state(gameplay__state);

    score = 0;
    multiplier = 1;

    uart_e connected_uart = song__mp3;
    uint8_t selected_song = get__selected_song();
    uint16_t song_runtime__seconds = 0;
    uint16_t song_start_delay__ms = 0;
    switch (selected_song) {
    case 1:
      song_runtime__seconds = 173;
      shift_row_delay = 40;
      song_start_delay__ms = 1700;
      mp3_module__play(song__mp3, weightless);
      break;

    case 2:
      song_runtime__seconds = 151;
      shift_row_delay = 35;
      song_start_delay__ms = 1400;
      mp3_module__play(song__mp3, dont_let_me);
      break;

    case 3:
      song_runtime__seconds = 255;
      shift_row_delay = 25;
      song_start_delay__ms = 850;
      mp3_module__play(song__mp3, neverafter);
      break;
    }

    vTaskDelay(2000);

    resume_tasks_for_gameplay(selected_song);
    draw__lane(Green);
    wait_for_arrows_to_reach_top_then_start_music(song_start_delay__ms, connected_uart, selected_song);

    wait_for_song_to_finish(song_runtime__seconds);

    mp3_module__pause(song__mp3);

    vTaskSuspend(beatmap__weightless__handler);
    vTaskSuspend(beatmap__dont_sleep__handler);
    vTaskSuspend(beatmap__neverafter__handler);

    vTaskSuspend(shift_matrix_handler);
    vTaskSuspend(draw_score_handler);
    vTaskSuspend(sound_effect_handler);

    vTaskDelay(1500);
    clear_screen_bottom_to_top();
    led_driver__clear_board();
    vTaskDelay(1500);
    suspend_game_logic_main_task_and_resume_song_select_task();
  }
}

void shift_matrix(void *p) {
  while (1) {
    erase_arrow_outlines();
    led_driver__shift__whole_matrix_up_a_row();
    draw_arrow_outlines(Red);
    vTaskDelay(shift_row_delay);
  }
}

void update_score(void *p) {
  while (1) {
    clear_score();
    draw_score();
    draw_multiplier();
    vTaskDelay(100);
  }
}

void sleep_on_semaphore_sound_effect(void *p) {
  while (1) {
    if (xSemaphoreTake(switch_pressed_signal_sound, portMAX_DELAY)) {
      play_random_fail_sound();
    }
  }
}

/***** HELPER FUNCTIONS BEGIN *****/
static void draw_arrow_outlines(color_code color) {
  draw__left_arrow_outline__static(color);
  draw__up_arrow_outline__static(color);
  draw__down_arrow_outline__static(color);
  draw__right_arrow_outline__static(color);
}

static void erase_arrow_outlines(void) {
  erase__left_arrow_outline__static();
  erase__up_arrow_outline__static();
  erase__down_arrow_outline__static();
  erase__right_arrow_outline__static();
}

static void resume_tasks_for_gameplay(uint8_t song_choice) {
  vTaskResume(shift_matrix_handler);
  vTaskResume(draw_score_handler);
  vTaskResume(sound_effect_handler);

  switch (song_choice) {
  case 1:
    vTaskResume(beatmap__weightless__handler);
    break;

  case 2:
    vTaskResume(beatmap__dont_sleep__handler);
    break;

  case 3:
    vTaskResume(beatmap__neverafter__handler);
    break;
  }
}

static void wait_for_arrows_to_reach_top_then_start_music(uint32_t time_to_wait__ms, uart_e uart_number,
                                                          uint8_t song_number) {
  vTaskDelay(time_to_wait__ms);
  mp3_module__resume(song__mp3);
}

static void wait_for_song_to_finish(uint16_t song_runtime__seconds) {
  const uint32_t song_runtime__ms = song_runtime__seconds * 1000;
  vTaskDelay(song_runtime__ms);
}

static void suspend_game_logic_main_task_and_resume_song_select_task(void) {
  vTaskResume(song_select_sequence);
  vTaskSuspend(game_logic_sequence);
}

static void clear_score(void) {
  led_driver__draw_partial_row(0, 0, 63, Black);
  led_driver__draw_partial_row(1, 0, 63, Black);
  led_driver__draw_partial_row(2, 0, 63, Black);
  led_driver__draw_partial_row(3, 0, 63, Black);
  led_driver__draw_partial_row(4, 0, 63, Black);
}

static void draw_score(void) {
  uint8_t row_index = 0;
  uint8_t col_index = 32;
  const color_code color = White;
  uint32_t local_score = score;
  const uint8_t number_of_digits_for_score = 4;
  for (uint8_t i = 0; i < number_of_digits_for_score; i++) {
    uint32_t digit = local_score % 10;
    local_score /= 10;

    switch (digit) {
    case 0:
      draw__0_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 1:
      draw__1_small(row_index, col_index, color);
      col_index -= 3;
      break;

    case 2:
      draw__2_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 3:
      draw__3_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 4:
      draw__4_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 5:
      draw__5_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 6:
      draw__6_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 7:
      draw__7_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 8:
      draw__8_small(row_index, col_index, color);
      col_index -= 5;
      break;

    case 9:
      draw__9_small(row_index, col_index, color);
      col_index -= 5;
      break;
    }
  }
}

static void draw_multiplier(void) {
  uint8_t row_index = 0;
  uint8_t col_index = 53;
  const color_code color = White;

  // X4
  draw__X_small(row_index, col_index, color);
  col_index += 5;

  switch (multiplier) {
  case 1:
    draw__1_small(row_index, col_index, color);
    break;

  case 2:
    draw__2_small(row_index, col_index, color);
    break;

  case 3:
    draw__3_small(row_index, col_index, color);
    break;

  case 4:
    draw__4_small(row_index, col_index, color);
    break;

  default:
    draw__1_small(row_index, col_index, color);
  }
}

bool determine_score_if_pixel_detected__handler(uint32_t pixel_y, uint32_t pixel_x) {
  bool pixel_status = false;
  pixel_status = led_driver__check_if_pixel_is_filled(pixel_y, pixel_x);
  update_score_and_multiplier(pixel_status);
  return pixel_status;
}

/**
 * @brief Include the multiplier_count until 10, then include multiplier
 *        If true, pixel hit, include score*multiplier, include multiplier_count
 *        If false, pixel missed, decrement score, reset multiplier/multiplier_count
 */
static void update_score_and_multiplier(bool is_hit) {
  static uint32_t multiplier_count = 0;
  const uint8_t max_multiplier = 4;
  const uint8_t multiplier_combo = 10;

  if (is_hit) {
    score += multiplier;
    ++multiplier_count;
    if (multiplier_count == multiplier_combo && multiplier < max_multiplier) {
      ++multiplier;
      multiplier_count = 0;
    } else if (multiplier_count == multiplier_combo) { // max multiplier/multiplier_count
      --multiplier_count;
    }
  } else {
    xSemaphoreGiveFromISR(switch_pressed_signal_sound, NULL);
    if (score != 0) {
      --score;
    }
    multiplier = 1;
    multiplier_count = 0;
  }
}

static void clear_screen_bottom_to_top(void) {
  for (uint8_t i = 63; i >= 1; i--) {
    led_driver__draw_row(i, Black);
    vTaskDelay(25);
  }
}
/***** HELPER FUNCTIONS END *****/
