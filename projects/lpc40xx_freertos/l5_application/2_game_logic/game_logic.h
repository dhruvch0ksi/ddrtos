#pragma once

#include "FreeRTOS.h"
#include "task.h"

#include "led_matrix.h"

TaskHandle_t game_logic_sequence;
TaskHandle_t shift_matrix_handler;
TaskHandle_t draw_score_handler;
TaskHandle_t sound_effect_handler;

void game_logic(void *p);
void shift_matrix(void *p);
void update_score(void *p);
void sleep_on_semaphore_sound_effect(void *p);

bool determine_score_if_pixel_detected__handler(uint32_t pixel_y, uint32_t pixel_x);
