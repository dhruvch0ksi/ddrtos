#include "button.h"

#include "arrow.h"
#include "game_logic.h"
#include "l3_drivers/gpio.h"
#include "l4_io/gpio0_isr.h"
#include "song_select.h"
#include "title_sequence.h"

#include <stdbool.h>
#include <stdio.h>

static game_state_e current_game_state;

game_state_e get_game_state(void) { return current_game_state; }

void set__game_state(game_state_e next_game_state) { current_game_state = next_game_state; }

/***** BUTTON HANDLER INITS ******/
#include "clock.h"
#include "hw_timer.h"

static const uint32_t debounce_delay__ms = 175;
static const lpc_timer_e hw_timer_port_2 = LPC_TIMER__2;
static uint32_t hw_timer_current_time = 0;
static uint32_t left_button_last_pressed_time = 0;
static uint32_t up_button_last_pressed_time = 0;
static uint32_t down_button_last_pressed_time = 0;
static uint32_t right_button_last_pressed_time = 0;
static const uint32_t sys_time__ms_per_sec = UINT32_C(1) * 1000;

static const uint8_t left_button_pin = 15;
static const uint8_t up_button_pin = 17;
static const uint8_t down_button_pin = 18;
static const uint8_t right_button_pin = 22;
static bool button_status(uint8_t pin) { return (LPC_GPIO0->PIN & (1 << pin)); }

static void reset(void) {
  LPC_SC->RSTCON0 = 0;
  LPC_SC->RSTCON1 = 0;
  NVIC_SystemReset();
}

static void check_reset(void) {
  if (button_status(left_button_pin) && button_status(up_button_pin) && button_status(right_button_pin) &&
      button_status(down_button_pin)) {
    reset();
  }
}

static bool is_debounce_button(uint32_t *button_last_pressed_time);

static void left_button__set_as_input(void) {
  gpio_s gpio = gpio__construct_as_input(0, 15);
  gpio__enable_pull_down_resistors(gpio);
}

static void up_button__set_as_input(void) {
  gpio_s gpio = gpio__construct_as_input(0, 17);
  gpio__enable_pull_down_resistors(gpio);
}

static void down_button__set_as_input(void) {
  gpio_s gpio = gpio__construct_as_input(0, 18);
  gpio__enable_pull_down_resistors(gpio);
}

static void right_button__set_as_input(void) {
  gpio_s gpio = gpio__construct_as_input(0, 22);
  gpio__enable_pull_down_resistors(gpio);
}

static void all_buttons__set_as_input() {
  up_button__set_as_input();
  down_button__set_as_input();
  right_button__set_as_input();
  left_button__set_as_input();
}

static void left_button__handler(void) {
  const uint8_t arrow_center_y = 11;
  const uint8_t arrow_center_x = 13;

  if (!is_debounce_button(&left_button_last_pressed_time)) {
    check_reset();

    switch (current_game_state) {
    case title__state:
      press_to_start__handler();
      break;

    case song_select__state:
      break;

    case song_select_wait__state:
      break;

    case gameplay__state:
      if (determine_score_if_pixel_detected__handler(arrow_center_y, arrow_center_x)) {
        clear_left_arrow_after_hit();
      }
      break;

    default:
      fprintf(stderr, "Err: Left Default game state\n");
      break;
    }
  }
}

static void up_button__handler(void) {
  const uint8_t arrow_center_y = 11;
  const uint8_t arrow_center_x = 25;

  if (!is_debounce_button(&up_button_last_pressed_time)) {
    check_reset();

    switch (current_game_state) {
    case title__state:
      press_to_start__handler();
      break;

    case song_select__state:
      song_menu_move_selector_up__handler();
      break;

    case song_select_wait__state:
      break;

    case gameplay__state:
      if (determine_score_if_pixel_detected__handler(arrow_center_y, arrow_center_x)) {
        clear_up_arrow_after_hit();
      }
      break;

    default:
      fprintf(stderr, "Err: Up Default game state\n");
      break;
    }
  }
}

static void down_button__handler(void) {
  const uint8_t arrow_center_y = 11;
  const uint8_t arrow_center_x = 36;

  if (!is_debounce_button(&down_button_last_pressed_time)) {
    check_reset();

    switch (current_game_state) {
    case title__state:
      press_to_start__handler();
      break;

    case song_select__state:
      song_menu_move_selector_down__handler();
      break;

    case song_select_wait__state:
      break;

    case gameplay__state:
      if (determine_score_if_pixel_detected__handler(arrow_center_y, arrow_center_x)) {
        clear_down_arrow_after_hit();
      }
      break;

    default:
      fprintf(stderr, "Err: Down Default game state\n");
      break;
    }
  }
}

static void right_button__handler(void) {
  const uint8_t arrow_center_y = 11;
  const uint8_t arrow_center_x = 49;

  if (!is_debounce_button(&right_button_last_pressed_time)) {
    check_reset();

    switch (current_game_state) {
    case title__state:
      press_to_start__handler();
      break;

    case song_select__state:
      song_menu_confirm_song_choice__handler();
      break;

    case song_select_wait__state:
      break;

    case gameplay__state:
      if (determine_score_if_pixel_detected__handler(arrow_center_y, arrow_center_x)) {
        clear_right_arrow_after_hit();
      }
      break;

    default:
      fprintf(stderr, "Err: Right Default game state\n");
      break;
    }
  }
}

static void all_buttons__init_interrupts() {
  const uint32_t prescalar_for_1us = ((clock__get_peripheral_clock_hz() / sys_time__ms_per_sec) - 1);
  hw_timer__enable(hw_timer_port_2, prescalar_for_1us, NULL);

  gpio0__attach_interrupt(17, GPIO_INTR__RISING_EDGE, up_button__handler);
  gpio0__attach_interrupt(18, GPIO_INTR__RISING_EDGE, down_button__handler);
  gpio0__attach_interrupt(22, GPIO_INTR__RISING_EDGE, right_button__handler);
  gpio0__attach_interrupt(15, GPIO_INTR__RISING_EDGE, left_button__handler);
}

static bool is_debounce_button(uint32_t *button_last_pressed_time) {
  // updates whichever button that is passed in by reference and returns whether:
  // debounced == true : don't do a button press if less than the debounce_delay
  // debounced == false: do a button press if more than the debounce_delay
  bool debounced = true;
  hw_timer_current_time = hw_timer__get_value(hw_timer_port_2);

  if ((hw_timer_current_time - *button_last_pressed_time) > debounce_delay__ms) {
    *button_last_pressed_time = hw_timer_current_time;
    debounced = false;
  }
  return debounced;
}

void all_buttons__init(void) {
  all_buttons__set_as_input();
  all_buttons__init_interrupts();
}