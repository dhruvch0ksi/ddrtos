#pragma once

typedef enum {
  title__state,
  song_select__state,
  song_select_wait__state,
  gameplay__state,
} game_state_e;

game_state_e get__game_state(void);
void set__game_state(game_state_e next_game_state);

void all_buttons__init(void);