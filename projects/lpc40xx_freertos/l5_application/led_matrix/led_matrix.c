#include "led_matrix.h"

#include "FreeRTOS.h"
#include "l3_drivers/gpio.h"
#include "task.h"

#include <stdio.h>

/***** HELPER FUNCTIONS *****/
static void clock_data(void);
static void enable_latch(void);
static void disable_latch(void);
static void enable_output(void);
static void disable_output(void);
static void clear_previously_selected_address(void);
static void set_all_matrix_data_pins_to_gpio_function_0(void);
static void set_all_matrix_pins_as_output(void);
static void set_all_matrix_pins_to_low(void);
static bool set_new_address_based_on_current_row(uint8_t row);
static bool choose_color_for_rows_0_through_31(color_code color);
static bool choose_color_for_rows_32_through_61(color_code color);

static volatile uint8_t led_matrix[64][64]; // volatile because multiple tasks can alter simultaneously

static gpio_s R1;
static gpio_s G1;
static gpio_s B1;
static gpio_s R2;
static gpio_s G2;
static gpio_s B2;
static gpio_s E;
static gpio_s A;
static gpio_s B;
static gpio_s C;
static gpio_s D;
static gpio_s CLK;
static gpio_s LAT;
static gpio_s OE;

void led_driver__matrix_init(void) {
  set_all_matrix_data_pins_to_gpio_function_0();
  set_all_matrix_pins_as_output();
  set_all_matrix_pins_to_low();
}

void led_driver__update_display_from_matrix(void) {
  for (uint8_t row = 0; row < 32; row++) {
    for (uint8_t col = 0; col < 64; col++) {
      choose_color_for_rows_0_through_31(led_matrix[row][col]);
      choose_color_for_rows_32_through_61(led_matrix[row + 32][col]);
      clock_data();
    }

    enable_latch();
    disable_output();

    clear_previously_selected_address();
    set_new_address_based_on_current_row(row);

    disable_latch();
    enable_output();
  }
}

void led_driver__draw_pixel(uint8_t row, uint8_t col, color_code color) { led_matrix[row][col] = color; }

void led_driver__draw_row(uint8_t row, color_code color) {
  for (uint8_t col = 0; col < 64; col++) {
    led_matrix[row][col] = color;
  }
}

void led_driver__draw_partial_row(uint8_t row, uint8_t col_start, uint8_t col_end, color_code color) {
  for (uint8_t col = col_start; col < col_end + 1; col++) {
    led_matrix[row][col] = color;
  }
}

void led_driver__draw_partial_row_adjust_speed(uint8_t row, uint8_t col_start, uint8_t col_end, color_code color,
                                               uint8_t delay__ms) {
  for (uint8_t col = col_start; col < col_end + 1; col++) {
    led_matrix[row][col] = color;
    vTaskDelay(delay__ms);
  }
}

void led_driver__draw_col(uint8_t col, color_code color) {
  for (int row = 0; row < 64; row++) {
    led_matrix[row][col] = color;
  }
}

void led_driver__draw_partial_col(uint8_t col, uint8_t row_start, uint8_t row_end, color_code color) {
  for (uint8_t row = row_start; row < row_end + 1; row++) {
    led_matrix[row][col] = color;
  }
}

void led_driver__draw_partial_col_adjust_speed(uint8_t col, uint8_t row_start, uint8_t row_end, color_code color,
                                               uint8_t delay__ms) {
  for (uint8_t row = row_start; row < row_end + 1; row++) {
    led_matrix[row][col] = color;
    vTaskDelay(delay__ms);
  }
}

void led_driver__draw_board(color_code color) {
  for (uint8_t row = 0; row < 64; row++) {
    for (uint8_t col = 0; col < 64; col++) {
      led_matrix[row][col] = color;
    }
  }
}

void led_driver__clear_board(void) {
  for (uint8_t row = 0; row < 64; row++) {
    for (uint8_t col = 0; col < 64; col++) {
      led_matrix[row][col] = Black;
    }
  }
}

void led_driver__shift__whole_matrix_up_a_row(void) {
  for (uint8_t row = 8; row <= 63; row++) {
    for (uint8_t col = 0; col < 64; col++) {
      if (col != 8 && col != 20 && col != 31 && col != 42 && col != 54) {
        led_matrix[row - 1][col] = led_matrix[row][col];
      }
    }
  }

  for (uint8_t col = 0; col < 64; col++) {
    led_matrix[63][col] = Black;
  }
}

void led_driver__shift__whole_matrix_down_a_row(void) {
  for (uint8_t row = 46; row > 0; row--) {
    for (uint8_t col = 0; col < 64; col++) {
      led_matrix[row + 1][col] = led_matrix[row][col];
    }
  }

  for (uint8_t col = 0; col < 64; col++) {
    led_matrix[0][col] = Black;
  }
}

bool led_driver__check_if_pixel_is_filled(uint8_t row, uint8_t col) {
  bool isFilled = false;
  if (led_matrix[row][col] != Black) {
    isFilled = true;
  }

  return isFilled;
}

/********** HELPER FUNCTIONS BEGIN **********/
static void clock_data() {
  gpio__set(CLK);
  gpio__reset(CLK);
}

static void enable_latch(void) { gpio__set(LAT); }

static void disable_latch(void) { gpio__reset(LAT); }

static void enable_output(void) {
  gpio__reset(OE); // Active low
}

static void disable_output(void) {
  gpio__set(OE); // Active low
}

static void clear_previously_selected_address(void) {
  gpio__reset(A);
  gpio__reset(B);
  gpio__reset(C);
  gpio__reset(D);
  gpio__reset(E);
}

static void set_all_matrix_data_pins_to_gpio_function_0(void) {
  R1 = gpio__construct_with_function(GPIO__PORT_0, 6, GPIO__FUNCTION_0_IO_PIN);
  G1 = gpio__construct_with_function(GPIO__PORT_0, 7, GPIO__FUNCTION_0_IO_PIN);
  B1 = gpio__construct_with_function(GPIO__PORT_0, 8, GPIO__FUNCTION_0_IO_PIN);
  // open pin at 0.7 because connection routes to GND

  R2 = gpio__construct_with_function(GPIO__PORT_0, 26, GPIO__FUNCTION_0_IO_PIN); //
  G2 = gpio__construct_with_function(GPIO__PORT_0, 25, GPIO__FUNCTION_0_IO_PIN); //
  B2 = gpio__construct_with_function(GPIO__PORT_1, 31, GPIO__FUNCTION_0_IO_PIN); //
  E = gpio__construct_with_function(GPIO__PORT_1, 30, GPIO__FUNCTION_0_IO_PIN);

  A = gpio__construct_with_function(GPIO__PORT_1, 20, GPIO__FUNCTION_0_IO_PIN);
  B = gpio__construct_with_function(GPIO__PORT_1, 23, GPIO__FUNCTION_0_IO_PIN);
  C = gpio__construct_with_function(GPIO__PORT_1, 28, GPIO__FUNCTION_0_IO_PIN);
  D = gpio__construct_with_function(GPIO__PORT_1, 29, GPIO__FUNCTION_0_IO_PIN);

  CLK = gpio__construct_with_function(GPIO__PORT_2, 0, GPIO__FUNCTION_0_IO_PIN);
  LAT = gpio__construct_with_function(GPIO__PORT_2, 1, GPIO__FUNCTION_0_IO_PIN);
  OE = gpio__construct_with_function(GPIO__PORT_2, 2, GPIO__FUNCTION_0_IO_PIN); // ACTIVE LOW
  // open pin at 2.1 because connection routes to GND
}

static void set_all_matrix_pins_as_output(void) {
  R1 = gpio__construct_as_output(R1.port_number, R1.pin_number);
  G1 = gpio__construct_as_output(G1.port_number, G1.pin_number);
  B1 = gpio__construct_as_output(B1.port_number, B1.pin_number);
  R2 = gpio__construct_as_output(R2.port_number, R2.pin_number);
  G2 = gpio__construct_as_output(G2.port_number, G2.pin_number);
  B2 = gpio__construct_as_output(B2.port_number, B2.pin_number);
  E = gpio__construct_as_output(E.port_number, E.pin_number);
  A = gpio__construct_as_output(A.port_number, A.pin_number);
  B = gpio__construct_as_output(B.port_number, B.pin_number);
  C = gpio__construct_as_output(C.port_number, C.pin_number);
  D = gpio__construct_as_output(D.port_number, D.pin_number);
  CLK = gpio__construct_as_output(CLK.port_number, CLK.pin_number);
  LAT = gpio__construct_as_output(LAT.port_number, LAT.pin_number);
  OE = gpio__construct_as_output(OE.port_number, OE.pin_number); // ACTIVE LOW
}

static void set_all_matrix_pins_to_low(void) {
  gpio__reset(R1);
  gpio__reset(G1);
  gpio__reset(B1);
  gpio__reset(R2);
  gpio__reset(G2);
  gpio__reset(B2);
  gpio__reset(E);
  gpio__reset(A);
  gpio__reset(B);
  gpio__reset(C);
  gpio__reset(D);
  gpio__reset(CLK);
  gpio__reset(LAT);
  gpio__reset(OE);
}

static bool set_new_address_based_on_current_row(uint8_t row) {
  bool success = false;

  if (row > 31) {
    return success;
  } else {
    success = true;
  }

  if (row & 0b0001) {
    gpio__set(A);
  }

  if (row & 0b0010) {
    gpio__set(B);
  }

  if (row & 0b0100) {
    gpio__set(C);
  }

  if (row & 0b1000) {
    gpio__set(D);
  }

  if (row & 0b10000) {
    gpio__set(E);
  }

  return success;
}

static bool choose_color_for_rows_0_through_31(color_code color) {
  bool success = false;

  if ((color < 0) || (color > 7)) {
    return success;
  }

  switch (color) {
  case Black:
    gpio__reset(R1);
    gpio__reset(G1);
    gpio__reset(B1);
    success = true;
    break;

  case Blue:
    gpio__reset(R1);
    gpio__reset(G1);
    gpio__set(B1);
    success = true;
    break;

  case Green:
    gpio__reset(R1);
    gpio__set(G1);
    gpio__reset(B1);
    success = true;
    break;

  case Cyan:
    gpio__reset(R1);
    gpio__set(G1);
    gpio__set(B1);
    success = true;
    break;

  case Red:
    gpio__set(R1);
    gpio__reset(G1);
    gpio__reset(B1);
    success = true;
    break;

  case Purple:
    gpio__set(R1);
    gpio__reset(G1);
    gpio__set(B1);
    success = true;
    break;

  case Yellow:
    gpio__set(R1);
    gpio__set(G1);
    gpio__reset(B1);
    success = true;
    break;

  case White:
    gpio__set(R1);
    gpio__set(G1);
    gpio__set(B1);
    success = true;
    break;
  }

  return success;
}

static bool choose_color_for_rows_32_through_61(color_code color) {
  bool success = false;

  if ((color < 0) || (color > 7)) {
    return success;
  }

  switch (color) {
  case Black:
    gpio__reset(R2);
    gpio__reset(G2);
    gpio__reset(B2);
    success = true;
    break;

  case Blue:
    gpio__reset(R2);
    gpio__reset(G2);
    gpio__set(B2);
    success = true;
    break;

  case Green:
    gpio__reset(R2);
    gpio__set(G2);
    gpio__reset(B2);
    success = true;
    break;

  case Cyan:
    gpio__reset(R2);
    gpio__set(G2);
    gpio__set(B2);
    success = true;
    break;

  case Red:
    gpio__set(R2);
    gpio__reset(G2);
    gpio__reset(B2);
    success = true;
    break;

  case Purple:
    gpio__set(R2);
    gpio__reset(G2);
    gpio__set(B2);
    success = true;
    break;

  case Yellow:
    gpio__set(R2);
    gpio__set(G2);
    gpio__reset(B2);
    success = true;
    break;

  case White:
    gpio__set(R2);
    gpio__set(G2);
    gpio__set(B2);
    success = true;
    break;
  }

  return success;
}
/********** HELPER FUNCTIONS END **********/