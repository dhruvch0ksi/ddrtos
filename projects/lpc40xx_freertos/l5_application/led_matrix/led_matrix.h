#pragma once

#include <stdbool.h>
#include <stdio.h>

typedef enum { // Color combinations of a RGB LED, experiment with the values
  Black,
  Blue,
  Green,
  Cyan,
  Red,
  Purple,
  Yellow,
  White
} color_code;

void led_driver__matrix_init(void);
void led_driver__update_display_from_matrix(void);

void led_driver__draw_pixel(uint8_t row, uint8_t col, color_code color);

void led_driver__draw_row(uint8_t row, color_code color);
void led_driver__draw_partial_row(uint8_t row, uint8_t col_start, uint8_t col_end, color_code color);
void led_driver__draw_partial_row_adjust_speed(uint8_t row, uint8_t col_start, uint8_t col_end, color_code color,
                                               uint8_t delay__ms);

void led_driver__draw_col(uint8_t col, color_code color);
void led_driver__draw_partial_col(uint8_t col, uint8_t row_start, uint8_t row_end, color_code color);
void led_driver__draw_partial_col_adjust_speed(uint8_t col, uint8_t row_start, uint8_t row_end, color_code color,
                                               uint8_t delay__ms);

void led_driver__draw_board(color_code color);
void led_driver__clear_board(void);

void led_driver__shift__whole_matrix_up_a_row(void);
void led_driver__shift__whole_matrix_down_a_row(void);

bool led_driver__check_if_pixel_is_filled(uint8_t row, uint8_t col);