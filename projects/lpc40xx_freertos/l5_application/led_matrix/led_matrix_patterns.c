#include "led_matrix_patterns.h"

#include "FreeRTOS.h"
#include "task.h"
#include <stdlib.h>

#include "led_matrix.h"

void led_patterns__fill_rows_R_G_B_then_clear(void) {
  for (uint8_t i = 0; i < 25; i++) {
    for (uint8_t row = 0; row < 64; row++) {
      led_driver__draw_row(row, Red);
      vTaskDelay(1);
    }

    for (uint8_t row = 0; row < 64; row++) {
      led_driver__draw_row(row, Green);
      vTaskDelay(1);
    }

    for (uint8_t row = 0; row < 64; row++) {
      led_driver__draw_row(row, Blue);
      vTaskDelay(1);
    }
  }

  led_driver__clear_board();
}

void led_patterns__set_random_pixels_then_clear(void) {
  for (uint32_t i = 0; i < 5000; i++) {
    uint32_t random_row = rand() % 64;
    uint32_t random_col = rand() % 64;
    uint32_t random_color = (rand() % 7) + 1;
    led_driver__draw_pixel(random_row, random_col, random_color);
    vTaskDelay(1);
  }
  led_driver__clear_board();
}

void led_patterns__increasingly_fast_row_color_changes_then_clear(void) {
  uint32_t delay_count = 60;
  uint32_t random_color = (rand() % 7) + 1;

  while (delay_count > 0) {
    for (uint8_t row = 0; row < 64; row++) {
      led_driver__draw_row(row, random_color);
      vTaskDelay(delay_count);
    }
    delay_count -= 10;
    random_color = (rand() % 7) + 1;

    for (uint8_t row = 63; row > 0; row--) {
      led_driver__draw_row(row, Black);
      vTaskDelay(delay_count);
    }
    delay_count -= 10;
  }

  led_driver__clear_board();
}

void led_patterns__write_hi(void) {
  led_driver__draw_pixel(29, 26, White);
  led_driver__draw_pixel(30, 26, White);
  led_driver__draw_pixel(31, 26, White);
  led_driver__draw_pixel(32, 26, White);
  led_driver__draw_pixel(33, 26, White);
  led_driver__draw_pixel(31, 27, White);
  led_driver__draw_pixel(31, 28, White);
  led_driver__draw_pixel(29, 29, White);
  led_driver__draw_pixel(30, 29, White);
  led_driver__draw_pixel(31, 29, White);
  led_driver__draw_pixel(32, 29, White);
  led_driver__draw_pixel(33, 29, White);

  led_driver__draw_pixel(29, 32, White);
  led_driver__draw_pixel(31, 32, White);
  led_driver__draw_pixel(32, 32, White);
  led_driver__draw_pixel(33, 32, White);
}

void led_patterns__flash_square_124_bpm(void) {
  // 64 - 8 = 56, 56 rows to travel, see a note on screen for ~4 beats
  // 56 / 4 = 14. 14 rows per beat. 124 bpm = 0.484 bps. 0.484 * 14 = 6.774
  led_driver__draw_pixel(31, 31, White);
  led_driver__draw_pixel(31, 32, White);
  led_driver__draw_pixel(31, 33, White);

  led_driver__draw_pixel(32, 31, White);
  led_driver__draw_pixel(32, 32, White);
  led_driver__draw_pixel(32, 33, White);

  led_driver__draw_pixel(33, 31, White);
  led_driver__draw_pixel(33, 32, White);
  led_driver__draw_pixel(33, 33, White);

  vTaskDelay(250);

  led_driver__draw_pixel(31, 31, Black);
  led_driver__draw_pixel(31, 32, Black);
  led_driver__draw_pixel(31, 33, Black);

  led_driver__draw_pixel(32, 31, Black);
  led_driver__draw_pixel(32, 32, Black);
  led_driver__draw_pixel(32, 33, Black);

  led_driver__draw_pixel(33, 31, Black);
  led_driver__draw_pixel(33, 32, Black);
  led_driver__draw_pixel(33, 33, Black);

  vTaskDelay(484 - 250);
}