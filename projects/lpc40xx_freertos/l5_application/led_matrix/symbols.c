#include "symbols.h"

#include <string.h>

void draw__left_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_pixel(top_left_corner_row + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 1, top_left_corner_col + 2, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 1, top_left_corner_col + 8, color);
  led_driver__draw_partial_row(top_left_corner_row + 3, top_left_corner_col + 0, top_left_corner_col + 8, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 8, color);
  led_driver__draw_partial_row(top_left_corner_row + 5, top_left_corner_col + 1, top_left_corner_col + 8, color);
  led_driver__draw_partial_row(top_left_corner_row + 6, top_left_corner_col + 2, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 7, top_left_corner_col + 3, color);
}

void draw__up_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 3, top_left_corner_col + 4, color);
  led_driver__draw_partial_row(top_left_corner_row + 1, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 1, top_left_corner_col + 6, color);
  led_driver__draw_partial_row(top_left_corner_row + 3, top_left_corner_col + 0, top_left_corner_col + 7, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 5, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 6, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 7, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 8, top_left_corner_col + 2, top_left_corner_col + 5, color);
}

void draw__down_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 1, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 3, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 5, top_left_corner_col + 0, top_left_corner_col + 7, color);
  led_driver__draw_partial_row(top_left_corner_row + 6, top_left_corner_col + 1, top_left_corner_col + 6, color);
  led_driver__draw_partial_row(top_left_corner_row + 7, top_left_corner_col + 2, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 8, top_left_corner_col + 3, top_left_corner_col + 4, color);
}

void draw__right_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_pixel(top_left_corner_row + 0, top_left_corner_col + 5, color);
  led_driver__draw_partial_row(top_left_corner_row + 1, top_left_corner_col + 5, top_left_corner_col + 6, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 7, color);
  led_driver__draw_partial_row(top_left_corner_row + 3, top_left_corner_col + 0, top_left_corner_col + 8, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 8, color);
  led_driver__draw_partial_row(top_left_corner_row + 5, top_left_corner_col + 0, top_left_corner_col + 7, color);
  led_driver__draw_partial_row(top_left_corner_row + 6, top_left_corner_col + 5, top_left_corner_col + 6, color);
  led_driver__draw_pixel(top_left_corner_row + 7, top_left_corner_col + 5, color);
}

void draw__left_arrow_outline__static(color_code color) {
  led_driver__draw_partial_row(7, 12, 14, color);
  led_driver__draw_pixel(8, 14, color);
  led_driver__draw_partial_row(9, 14, 19, color);
  led_driver__draw_partial_col(19, 10, 13, color);
  led_driver__draw_partial_row(14, 14, 19, color);
  led_driver__draw_pixel(15, 14, color);
  led_driver__draw_partial_row(16, 12, 14, color);
  led_driver__draw_partial_row(15, 11, 12, color);
  led_driver__draw_partial_row(14, 10, 11, color);
  led_driver__draw_partial_row(13, 9, 10, color);
  led_driver__draw_partial_col(9, 11, 12, color);
  led_driver__draw_partial_row(10, 9, 10, color);
  led_driver__draw_partial_row(9, 10, 11, color);
  led_driver__draw_partial_row(8, 11, 12, color);
}

void erase__left_arrow_outline__static() {
  led_driver__draw_partial_row(7, 12, 14, Black);
  led_driver__draw_pixel(8, 14, Black);
  led_driver__draw_partial_row(9, 14, 19, Black);
  led_driver__draw_partial_col(19, 10, 13, Black);
  if (!led_driver__check_if_pixel_is_filled(15, 15)) {
    led_driver__draw_partial_row(14, 14, 19, Black);
  }

  led_driver__draw_pixel(15, 14, Black);

  if (!led_driver__check_if_pixel_is_filled(17, 13)) {
    led_driver__draw_partial_row(16, 12, 14, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(15, 13)) {
    led_driver__draw_partial_row(15, 11, 12, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(14, 12)) {
    led_driver__draw_partial_row(14, 10, 11, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(13, 11)) {
    led_driver__draw_partial_row(13, 9, 10, Black);
  }

  led_driver__draw_partial_col(9, 11, 12, Black);
  led_driver__draw_partial_row(10, 9, 10, Black);
  led_driver__draw_partial_row(9, 10, 11, Black);
  led_driver__draw_partial_row(8, 11, 12, Black);
}

void draw__up_arrow_outline__static(color_code color) {
  led_driver__draw_partial_row(6, 24, 27, color);
  led_driver__draw_partial_row(7, 27, 28, color);
  led_driver__draw_partial_row(8, 28, 29, color);
  led_driver__draw_partial_row(9, 29, 30, color);
  led_driver__draw_pixel(10, 30, color);
  led_driver__draw_partial_row(11, 28, 30, color);
  led_driver__draw_partial_col(28, 12, 15, color);
  led_driver__draw_partial_row(16, 23, 28, color);
  led_driver__draw_partial_col(23, 12, 15, color);
  led_driver__draw_partial_row(11, 21, 23, color);
  led_driver__draw_pixel(10, 21, color);
  led_driver__draw_partial_row(9, 21, 22, color);
  led_driver__draw_partial_row(8, 22, 23, color);
  led_driver__draw_partial_row(7, 23, 24, color);
}

void erase__up_arrow_outline__static(void) {
  led_driver__draw_partial_row(6, 24, 27, Black);
  led_driver__draw_partial_row(7, 27, 28, Black);
  led_driver__draw_partial_row(8, 28, 29, Black);
  led_driver__draw_partial_row(9, 29, 30, Black);
  led_driver__draw_pixel(10, 30, Black);

  if (!led_driver__check_if_pixel_is_filled(12, 29)) {
    led_driver__draw_partial_row(11, 28, 30, Black);
  }

  led_driver__draw_partial_col(28, 12, 15, Black);

  if (!led_driver__check_if_pixel_is_filled(17, 27)) {
    led_driver__draw_partial_row(16, 23, 28, Black);
  }

  led_driver__draw_partial_col(23, 12, 15, Black);

  if (!led_driver__check_if_pixel_is_filled(12, 22)) {
    led_driver__draw_partial_row(11, 21, 23, Black);
  }

  led_driver__draw_pixel(10, 21, Black);
  led_driver__draw_partial_row(9, 21, 22, Black);
  led_driver__draw_partial_row(8, 22, 23, Black);
  led_driver__draw_partial_row(7, 23, 24, Black);
}

void draw__down_arrow_outline__static(color_code color) {
  led_driver__draw_partial_row(6, 34, 39, color);
  led_driver__draw_partial_col(39, 7, 11, color);
  led_driver__draw_partial_row(11, 39, 41, color);
  led_driver__draw_pixel(12, 41, color);
  led_driver__draw_partial_row(13, 40, 41, color);
  led_driver__draw_partial_row(14, 39, 40, color);
  led_driver__draw_partial_row(15, 38, 39, color);
  led_driver__draw_partial_row(16, 35, 38, color);
  led_driver__draw_partial_row(15, 34, 35, color);
  led_driver__draw_partial_row(14, 33, 34, color);
  led_driver__draw_partial_row(13, 32, 33, color);
  led_driver__draw_pixel(12, 32, color);
  led_driver__draw_partial_row(11, 32, 34, color);
  led_driver__draw_partial_col(34, 6, 10, color);
}

void erase__down_arrow_outline__static(void) {
  led_driver__draw_partial_row(6, 34, 39, Black);
  led_driver__draw_partial_col(39, 7, 11, Black);
  led_driver__draw_partial_row(11, 39, 41, Black);
  led_driver__draw_pixel(12, 41, Black);

  if (!led_driver__check_if_pixel_is_filled(13, 39)) {
    led_driver__draw_partial_row(13, 40, 41, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(14, 38)) {
    led_driver__draw_partial_row(14, 39, 40, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(15, 37)) {
    led_driver__draw_partial_row(15, 38, 39, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(17, 37)) {
    led_driver__draw_partial_row(16, 35, 38, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(15, 36)) {
    led_driver__draw_partial_row(15, 34, 35, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(14, 35)) {
    led_driver__draw_partial_row(14, 33, 34, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(13, 34)) {
    led_driver__draw_partial_row(13, 32, 33, Black);
  }

  led_driver__draw_pixel(12, 32, Black);
  led_driver__draw_partial_row(11, 32, 34, Black);
  led_driver__draw_partial_col(34, 6, 10, Black);
}

void draw__right_arrow_outline__static(color_code color) {
  led_driver__draw_partial_row(7, 48, 50, color);
  led_driver__draw_partial_row(8, 50, 51, color);
  led_driver__draw_partial_row(9, 51, 52, color);
  led_driver__draw_partial_row(10, 52, 53, color);
  led_driver__draw_partial_col(53, 11, 12, color);
  led_driver__draw_partial_row(13, 52, 53, color);
  led_driver__draw_partial_row(14, 51, 52, color);
  led_driver__draw_partial_row(15, 50, 51, color);
  led_driver__draw_partial_row(16, 48, 50, color);
  led_driver__draw_pixel(15, 48, color);
  led_driver__draw_partial_row(14, 43, 48, color);
  led_driver__draw_partial_col(43, 10, 14, color);
  led_driver__draw_partial_row(9, 43, 48, color);
  led_driver__draw_pixel(8, 48, color);
}

void erase__right_arrow_outline__static(void) {
  led_driver__draw_partial_row(7, 48, 50, Black);
  led_driver__draw_partial_row(8, 50, 51, Black);
  led_driver__draw_partial_row(9, 51, 52, Black);
  led_driver__draw_partial_row(10, 52, 53, Black);
  led_driver__draw_partial_col(53, 11, 12, Black);
  if (!led_driver__check_if_pixel_is_filled(13, 51)) {
    led_driver__draw_partial_row(13, 52, 53, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(14, 50)) {
    led_driver__draw_partial_row(14, 51, 52, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(15, 49)) {
    led_driver__draw_partial_row(15, 50, 51, Black);
  }

  if (!led_driver__check_if_pixel_is_filled(17, 49)) {
    led_driver__draw_partial_row(16, 48, 50, Black);
  }

  led_driver__draw_pixel(15, 48, Black);

  if (!led_driver__check_if_pixel_is_filled(15, 47)) {
    led_driver__draw_partial_row(14, 43, 48, Black);
  }
  led_driver__draw_partial_col(43, 10, 14, Black);
  led_driver__draw_partial_row(9, 43, 48, Black);
  led_driver__draw_pixel(8, 48, Black);
}

void draw__left_arrow_bottom__static(color_code color) { draw__left_arrow(54, 10, color); }

void draw__up_arrow_bottom__static(color_code color) { draw__up_arrow(54, 22, color); }

void draw__down_arrow_bottom__static(color_code color) { draw__down_arrow(54, 33, color); }

void draw__right_arrow_bottom__static(color_code color) { draw__right_arrow(54, 44, color); }

void draw__title_letter_D(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 4, top_left_corner_row + 2, top_left_corner_row + 5, color);
  led_driver__draw_pixel(top_left_corner_row + 6, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 7, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 6, color);
}

void draw__title_letter_R(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 7, color);
  led_driver__draw_partial_col(top_left_corner_col + 4, top_left_corner_row + 1, top_left_corner_row + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 3, top_left_corner_col + 1, top_left_corner_col + 4, color);
  led_driver__draw_pixel(top_left_corner_row + 4, top_left_corner_col + 1, color);
  led_driver__draw_pixel(top_left_corner_row + 5, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 6, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 7, top_left_corner_col + 4, color);
}

void draw__title_letter_T(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 2, top_left_corner_row + 0, top_left_corner_row + 7, color);
}

void draw__title_letter_O(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 4, top_left_corner_row + 1, top_left_corner_row + 7, color);
  led_driver__draw_partial_row(top_left_corner_row + 7, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 6, color);
}

void draw__title_letter_S(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 3, top_left_corner_col + 0, top_left_corner_col + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 4, top_left_corner_row + 3, top_left_corner_row + 7, color);
  led_driver__draw_partial_row(top_left_corner_row + 7, top_left_corner_col + 0, top_left_corner_col + 4, color);
}

void draw__A_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 1, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 1, top_left_corner_row + 4, color);
}

void draw__B_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 3, color);
}

void draw__C_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 1, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 1, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 3, color);
}

void draw__D_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 1, top_left_corner_row + 3, color);
}

void draw__E_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__F_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__G_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 1, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 2, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 1, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 2, top_left_corner_row + 3, color);
}

void draw__H_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__I_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 1, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 2, color);
}

void draw__J_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, White);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 2, White);
  led_driver__draw_partial_col(top_left_corner_col + 2, top_left_corner_row + 0, top_left_corner_row + 4, White);
}

void draw__K_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_pixel(top_left_corner_row + 0, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 2, top_left_corner_col + 1, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 4, top_left_corner_col + 3, color);
}

void draw__L_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
}

void draw__M_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 4, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 1, color);
  led_driver__draw_pixel(top_left_corner_row + 2, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 3, color);
}

void draw__N_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 1, color);
  led_driver__draw_pixel(top_left_corner_row + 2, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__O_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 1, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 1, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 1, top_left_corner_row + 3, color);
}

void draw__P_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 2, color);
}

void draw__Q_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 1, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 1, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 1, top_left_corner_row + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 1, top_left_corner_row + 4, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 2, color);
}

void draw__R_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 1, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 2, top_left_corner_col + 3, color);
}

void draw__S_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 1, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 2, top_left_corner_row + 4, color);
}

void draw__T_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 2, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__U_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__V_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_pixel(top_left_corner_row + 4, top_left_corner_col + 1, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 2, top_left_corner_row + 0, top_left_corner_row + 3, color);
}

void draw__W_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 4, top_left_corner_row + 0, top_left_corner_row + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 4, top_left_corner_col + 1, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 4, top_left_corner_col + 3, color);
}

void draw__X_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 1, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 3, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 1, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 3, top_left_corner_row + 4, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 1, top_left_corner_col + 2, color);
}

void draw__Y_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
}

void draw__Z_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 2, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 1, color);
}

void draw__0_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__1_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 2, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__2_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 1, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 0, color);
}

void draw__3_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__4_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
}

void draw__5_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 3, White);
}

void draw__6_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 2, top_left_corner_row + 4, color);
}

void draw__7_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 1, top_left_corner_col + 3, color);
  led_driver__draw_pixel(top_left_corner_row + 2, top_left_corner_col + 2, color);
  led_driver__draw_pixel(top_left_corner_row + 3, top_left_corner_col + 1, color);
  led_driver__draw_pixel(top_left_corner_row + 4, top_left_corner_col + 0, color);
}

void draw__8_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 4, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__9_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 0, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_row(top_left_corner_row + 2, top_left_corner_col + 0, top_left_corner_col + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 2, color);
  led_driver__draw_partial_col(top_left_corner_col + 3, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

void draw__period(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_pixel(top_left_corner_row + 4, top_left_corner_col + 0, color);
}

void draw__selector_arrow(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_pixel(top_left_corner_row + 2, top_left_corner_col + 0, color);
  led_driver__draw_partial_col(top_left_corner_col + 1, top_left_corner_row + 1, top_left_corner_row + 3, color);
  led_driver__draw_partial_col(top_left_corner_col + 2, top_left_corner_row + 0, top_left_corner_row + 4, color);
}

static void draw__lane_left(color_code color) { led_driver__draw_partial_col(8, 6, 63, color); }
static void draw__lane_left_mid(color_code color) { led_driver__draw_partial_col(20, 6, 63, color); }
static void draw__lane_mid(color_code color) { led_driver__draw_partial_col(31, 6, 63, color); }
static void draw__lane_right_mid(color_code color) { led_driver__draw_partial_col(42, 6, 63, color); }
static void draw__lane_right(color_code color) { led_driver__draw_partial_col(54, 6, 63, color); }

void draw__lane(color_code color) {
  draw__lane_left(color);
  draw__lane_left_mid(color);
  draw__lane_mid(color);
  draw__lane_right_mid(color);
  draw__lane_right(color);
}

void draw__lowercase_i_small(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_pixel(top_left_corner_row + 0, top_left_corner_col + 0, color);
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 2, top_left_corner_row + 4, color);
}

void draw__apostrophe(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_col(top_left_corner_col + 0, top_left_corner_row + 0, top_left_corner_row + 1, color);
}

void draw__underscore(uint8_t top_left_corner_row, uint8_t top_left_corner_col, color_code color) {
  led_driver__draw_partial_row(top_left_corner_row + 4, top_left_corner_col + 0, top_left_corner_col + 2, color);
}
